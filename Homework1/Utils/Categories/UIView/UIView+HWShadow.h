//
//  UIView+HWShadow.h
//  HelpWill
//
//  Created by Viktor Peschenkov on 06.07.16.
//  Copyright © 2016 INOSTUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (HWShadow)

- (void)hw_setShadow;
- (void)hw_setShadow:(UIColor *)color offset:(CGSize)offset opacity:(CGFloat)opacity radius:(CGFloat)radius;
- (void)hw_removeShadow;

@end

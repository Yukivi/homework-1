//
//  HWAnimationView.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 28/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSUInteger const HWAnimationViewDefaultPointsThreshold;
extern CGFloat const HWAnimationViewDefaultPointSize;
extern NSInteger const HWAnimationViewDefaultPointColorHex;

@interface HWAnimationView : UIView

@property (nonatomic, assign) NSUInteger pointsThreshold;
@property (nonatomic, strong) UIColor *pointColor;
@property (nonatomic, assign) CGFloat pointSize;

- (void)addPoint:(CGPoint)point;

@end
